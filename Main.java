package Maze;

import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
	private static Maze maze;
	
	private static Player player;
	
	private static Treasure treasure;
	
	private static int[] dimensions;
	
	public static void main(String[] args) 
	{
		Init();
		Game();
		Close();
	}

	public static void Init()
	{
		SetSize();
		CreateTreasure();
		CreatePlayer();
		ArrayList<ITangible> tangibles = CreateBlocks();
		tangibles.add(treasure);
		tangibles.add(player);
		CreateMaze(tangibles);
	}
	
	public static void Game()
	{
		while (!HaveWon())
		{
			maze.PrintMaze(maze.BuildMaze());
			MovePlayer();
		}
	}
	
	public static void Close()
	{
		System.out.println("Congratulations, " + player.getName() + "! S.Y");
	}
	
	//INIT FUNCTIONS
	
	//Creates the full maze.
	public static void CreateMaze(ArrayList<ITangible> tangibles)
	{
		maze = new Maze(dimensions[0], dimensions[1], tangibles);
	}
	
	//Initialize list of blocks.
	public static ArrayList<ITangible> CreateBlocks()
	{
		int numberOfBlocks = (int)(Math.random() * ((dimensions[0] * dimensions[1])/3));
		ArrayList<ITangible> blockList = new ArrayList<ITangible>();
		for (int i=0; i < numberOfBlocks; i++)
		{
			int x = (int)(Math.random() * dimensions[0]);
			int y = (int)(Math.random() * dimensions[1]);
			Point blockPoint= new Point(x,y);
			boolean taken = false;
			for (int j = 0; j < blockList.size(); j++)
			{
				if (blockList.get(j).getPoint().equals(blockPoint))
				{
					taken = true;
					break;
				}	
			}
			if (blockPoint.equals(player.getPoint()))
			{
				taken = true;
			}
			if (blockPoint.equals(treasure.getPoint()))
			{
				taken = true;
			}
			if (!taken)
			{
				blockList.add(new Block(blockPoint));
			}
		}
		return blockList;
	}
	
	
	//Welcome message and set name.
	public static void CreatePlayer()
	{
		System.out.println("Welcome, adventurer! State your name.");
		Scanner reader = new Scanner(System.in);
		String name = reader.next();
		int x = (int)(Math.random() * dimensions[0]);
		int y = (int)(Math.random() * dimensions[1]);
		Point initialPoint = new Point(x,y); 
		player = new Player(name, initialPoint);
	}
	
	//Set width and height, set player and treasure positions.
	public static void SetSize()
	{
		    dimensions = new int[2];	
		    dimensions[0] = (int)(Math.random() * 10 + 3);
			dimensions[1] = (int)(Math.random() * 10 + 3);
	}
	
	//Set Treasure position.
	public static void CreateTreasure()
	{
		int x = (int)(Math.random() * dimensions[0]);
		int y = (int)(Math.random() * dimensions[1]);
		Point initialPoint = new Point(x,y); 
		treasure = new Treasure(initialPoint);
	}
	
	
	//GAME FUNCTIONS
	
	public static boolean HaveWon()
	{
		return treasure.getPoint().equals(player.getPoint());
	}
	
	public static void MovePlayer()
	{
		System.out.println("Where would you like to go, " + player.getName() + "? (Use WASD and then enter.)");
		Scanner reader = new Scanner(System.in);
		String directionString = reader.next();
		char direction = directionString.charAt(0);
		switch (direction)
		{
		case 'W':
		case 'w':
			player.Move(new North());
			if (!MoveCheck())
			{
				player.Move(new South());
			}
			break;
		case 'A':
		case 'a':
			player.Move(new West());
			if (!MoveCheck())
			{
				player.Move(new East());
			}
			break;
		case 'S':
		case 's':
			player.Move(new South());
			if (!MoveCheck())
			{
				player.Move(new North());
			}
			break;
		case 'D':
		case 'd':
			player.Move(new East());
			if (!MoveCheck())
			{
				player.Move(new West());
			}
			break;
		}
	}
	
	public static boolean MoveCheck()
	{
		if(player.getPoint().getxPos() < 0 || player.getPoint().getxPos() >= dimensions[0])
		{
			return false;
		}
		
		if(player.getPoint().getyPos() < 0 || player.getPoint().getyPos() >= dimensions[1])
		{
			return false;
		}
		
		for (int i = 0; i < maze.getTangibles().size(); i++)
		{
			if (player != maze.getTangibles().get(i) && player.getPoint().equals(maze.getTangibles().get(i).getPoint()) && !player.getPoint().equals(treasure.getPoint()))
			{
				return false;
			}
		}
		return true;
	}
	
}
