package Maze;

public class Player extends ITangible
{
	private String name;
	
	public Player(String name, Point point)
	{
		this.symbol = '@';
		this.name = name;
		this.point = point;
	}
	
	public void Move(ITargetable d)
	{
		Point nextPoint = d.NextPoint(this.point);
		this.point.setxPos(nextPoint.getxPos());
		this.point.setyPos(nextPoint.getyPos());
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
