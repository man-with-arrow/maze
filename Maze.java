package Maze;
import java.util.ArrayList;

public class Maze 
{
	public Maze(int width, int height, ArrayList<ITangible> tangibles)
	{
		this.width = width;
		this.height = height;
		this.tangibles = tangibles;
	}
	
	private ArrayList<ITangible> tangibles;
	private int width, height;
	
	public char[][] BuildMaze()
	{
		char[][] printableMaze = new char[width][height];
		for (int i=0; i < tangibles.size(); i++)
		{
			ITangible tangible = tangibles.get(i);
			printableMaze[tangible.getPoint().getxPos()][tangible.getPoint().getyPos()] = tangible.getSymbol();
		}
		return printableMaze;
	}
	
	
	public ArrayList<ITangible> getTangibles()
	{
		return tangibles;
	}
	public void setTangibles(ArrayList<ITangible> tangibles)
	{
		this.tangibles = tangibles;
	}
	public int getWidth()
	{
		return width;
	}
	public void setWidth(int width)
	{
		this.width = width;
	}
	public int getHeight()
	{
		return height;
	}
	public void setHeight(int height)
	{
		this.height = height;
	}
	
	
	public void PrintMaze(char[][]printableMaze)
	{
		for (int i = 0; i < printableMaze.length; i++)
		{
			for (int j = 0; j < printableMaze[i].length; j++)
			{
				if (printableMaze[i][j] == '\0')
				{
					printableMaze[i][j] = '.';
				}
				System.out.print(printableMaze[i][j]);
			}
			System.out.println();
		}
	}
}
