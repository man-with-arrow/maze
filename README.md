# Maze
 
Java project, in which the player has to find a treasure in a maze. First actual foray into language that contains more than one class.

## What does everything do?

### ITangible
Class (used as an interface), from which all tangible objects in the maze inherit. Contains a point (defined in Point; x,y coordinates) and a symbol (which is printed to the console as a representation of the object).

#### Player
Inherits from ITangible, also possesses name and the function Move (changes x-position and y-position, is called in Main and used with NSEW).

#### Block
Inherits from ITangible. Blocks the player from progressing.

#### Treasure
Inherits from ITangible. If the player lands on it, the game ends.

### ITargetable
Class (used as an interface), inherits to NSEW in order to define player movement.

#### NSEW
Inherit from ITargetable, contain the function NextPoint (which either adds or subtracts 1 to x/y, according to the direction; and returns the new position.)

### Point
Used throughout the program, in order to define position of objects. Contains private ints xPos, yPos (self-explanatory), the function Point (which updates xPos and yPos), and the function equals (that checks if two points are equal).

### Maze
Defines different aspects of the maze itself. Constructs a new type - Maze - which has width, height, and contents. Contains the functions BuildMaze (which returns a two-dimensional array, built from an ArrayList) and PrintMaze (which goes over each row --> column, inserts a '.' if there is no object in the cell, and prints.)

### Main
The actual program. Contains Init, Game, and Close functions.

#### Init
##### SetSize
Randomly sets the maze's size (width and height).

##### CreateMaze
Creates the full maze.

##### CreateBlocks
Randomly initializes list of blocks

##### CreatePlayer
Sets the players name, and randomly sets the player's position.

##### CreateTreasure
Creates and randomly sets the treasure's position.

#### Game
##### HaveWon
Returns true if the player and the treasure are in the same position.

##### MovePlayer
Moves the player according to user input (W, A, S, D). Moves are verified using MoveCheck.

##### MoveCheck
Checks if the next position is OOB, or on a block. In which case, the next point is set as the current point.

#### Close
Prints out a "congrats" message.